﻿import QtQuick 2.0
import "../Basic"
import Pipeline 1.0

PageList0 {
    id: self
    property string name: ""
    tr: Pipeline.tr
    onSelected: {
        Pipeline.run(name + "_listViewSelected", [], "manual")
    }
    Component.onCompleted: {
        Pipeline.add(function(aInput){
            aInput.setData(selects).out()
        }, {name: name + "_listViewSelected", type: "Partial", vtype: "array"})

        Pipeline.add(function(aInput){
            updateModel(aInput.data())
            aInput.out()
        }, {name: name + "_updateListView"})

        Pipeline.add(function(aInput){
            var dt = aInput.data()
            self[dt["cmd"]](dt["param"])
        }, {name: name + "_callMethod"})
    }
}
