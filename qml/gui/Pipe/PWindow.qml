﻿import QtQuick 2.0
import "../Basic"
import Pipeline 1.0

PWindow0 {
    property string name: ""
    property string service_tag
    tr: Pipeline.tr

    onAccept: {
        close()
        Pipeline.run(name + "_objectNew", outputModel(), service_tag, false)
    }

    onReject: {
        close()
        Pipeline.run(name + "_objectNew", {}, service_tag, false)
    }

    Component.onCompleted: {
        Pipeline.add(function(aInput){
            aInput.out()
        }, {name: name + "_objectNew", type: "Partial"})

        Pipeline.add(function(aInput){
            service_tag = aInput.tag()
            showModel(aInput.data())
        }, {name: name + "_newObject", type: "Delegate", delegate: name + "_objectNew"})
    }
}
