import QtQuick 2.12
import QtQuick.Controls 2.5
import "../Basic"
import "../Style"
import Pipeline 1.0

TWindow{
    id: root
    property QtObject cstyle: DefaultStyle
    property QtObject windowStyle: WindowStyle
    width: cstyle.progress.width
    height: cstyle.progress.height
    caption: Pipeline.tr("progress")
    titlecolor: windowStyle.bodycolor
    content: Column{
        leftPadding: cstyle.progress.leftPadding(this)
        width: cstyle.progress.coWidth(this)
        height: cstyle.getHeight(this)
        spacing: cstyle.progress.spacing(this)
        Text{
            id: title
            text: ""
            font.pixelSize: cstyle.fontSize
            height: cstyle.progress.textHeight(this)
            width: cstyle.getWidth(this)
        }

        ProgressBar{
            property int sum
            property int cur
            property string tit
            id: bar
            height: cstyle.progress.getHeight(this)
            width: cstyle.getWidth(this)
            from: cstyle.progress.from
            to: cstyle.progress.to
            value: 0

            function updateProgress(aInfo){
                if (aInfo["title"]){
                    sum = aInfo["sum"]
                    cur = 0
                    tit = aInfo["title"]
                }else{
                    if (aInfo["step"] !== undefined){
                        if (aInfo["step"] < 0)
                            sum -= aInfo["step"]
                        else
                            cur += aInfo["step"]
                    }
                    else
                        ++cur
                }
                value = cur / sum
                title.text = tit + " (" + cur + "/" + sum + ")"

                if (value == 1){
                    root.close()
                }
                else if (!root.visible){
                    root.show()
                }
            }
            Component.onCompleted: {
                Pipeline.add(function(aInput){
                    updateProgress(aInput.data())
                    aInput.outs(value)
                }, {name: "updateProgress", type: "Partial"})
            }
        }
    }
}
