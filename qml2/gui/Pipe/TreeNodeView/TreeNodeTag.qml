import QtQuick 2.12
import QtQuick.Controls 2.5
import "../../Style"

Button{
    property string comment
    property QtObject cstyle: TreeviewStyle
    property QtObject defaultStyle: DefaultStyle

    text: "?"
    width: cstyle.button.width
    height: width

    onClicked: {
        detail.open()
    }
    Popup{
        id: detail
        Text{
            text: comment
            font.pixelSize: defaultStyle.fontSize
        }
    }
}
