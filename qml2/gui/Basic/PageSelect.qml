import QtQuick 2.12
import QtQuick.Controls 2.5
import '../Style'

FocusScope{
    id: fs
    property string page_text
    property string page_suffix
    property string page_title: "Page:"
    property QtObject cstyle: DefaultStyle
    property string background_color: cstyle.button.activeColor
    property QtObject pagestyle: PageStyle
    signal selectSpecificPage(var aIndex)
    signal selectNeighbourPage(var aNext)
    anchors.horizontalCenter: parent.horizontalCenter
    width: pagestyle.width
    height: cstyle.height
    Row{
        anchors.fill: parent
        Edit{
            caption.text: page_title
            input.text: fs.page_text
            input.font.pixelSize: cstyle.fontSize
            background.color: background_color
            width: pagestyle.edit.width
            ratio: pagestyle.edit.ratio
            input.onAccepted: {
                selectSpecificPage(parseInt(input.text))
                fs.focus = false
            }
        }
        Label{
            text: fs.page_suffix
            font.pixelSize: cstyle.fontSize
            color: cstyle.blackColor
            anchors.verticalCenter: cstyle.verticalCenter(this)
        }
        Button{
            background: Rectangle {
                color: cstyle.transparent
                border.color: pagestyle.button.backBorderColor
                radius: pagestyle.button.radius
            }
            anchors.verticalCenter: cstyle.verticalCenter(this)
            height: pagestyle.button.height
            width: pagestyle.button.width
            font.pixelSize: cstyle.fontSize
            text: "<"
            onClicked: selectNeighbourPage(false)
        }
        Button{
            background: Rectangle {
                color: cstyle.transparent
                border.color: pagestyle.button.backBorderColor
                radius: pagestyle.button.radius
            }
            anchors.verticalCenter: cstyle.verticalCenter(this)
            height: pagestyle.button.height
            width: pagestyle.button.width
            font.pixelSize: cstyle.fontSize
            text: ">"
            onClicked: selectNeighbourPage(true)
        }
    }
}
