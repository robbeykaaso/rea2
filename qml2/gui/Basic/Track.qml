import QtQuick 2.12
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import '../Style'

Row {
    property int interval: cstyle.slider.interval
    property double ratio: cstyle.ratio
    property alias caption: caption
    property alias slider: slider
    property QtObject cstyle: DefaultStyle
    signal indexChanged(int aIndex)

    width: cstyle.width
    height: cstyle.height

    spacing: cstyle.spacing(this)
    Text {
        id: caption
        text: "hello:"
        verticalAlignment: Text.AlignVCenter
        anchors.verticalCenter: cstyle.verticalCenter(this)
        font.pixelSize: cstyle.fontSize
        width: cstyle.text.width(this)
        height: cstyle.text.height(this)
    }

    Slider{
        id: slider
        property int last_idx
        width: cstyle.rect.width(this)
        height: cstyle.rect.height(this)
        anchors.verticalCenter: cstyle.verticalCenter(this)
        minimumValue: cstyle.slider.minimumValue
        maximumValue: cstyle.slider.maximumValue
        value: 0

        function updateInterval(){
            var idx = 0
            if (interval >= 1){
                var step = 100 / interval
                idx = Math.round(value / step)
                value = idx * step
            }
            if (last_idx !== idx)
                indexChanged(idx)
            last_idx = idx
        }

        onValueChanged: {
            updateInterval()
        }
        style: cstyle.slider.style
    }
}
