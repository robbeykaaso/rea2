import QtQuick 2.12
import QtQuick.Layouts 1.12
import '../Style'

Rectangle {
    property QtObject cstyle: DefaultStyle

    property var menu: ['1', '2', '3']
    signal updated()

    RowLayout {
        spacing: 1
        height: cstyle.getHeight(this)

        Repeater {
            model: menu
            Row {
                height: cstyle.getHeight(this)
                leftPadding: cstyle.margins
                focus: cstyle.focus
                id: _row
                Text {
                    id: control
                    color: cstyle.blackColor
                    height: cstyle.height
                    font.pixelSize: cstyle.fontSize
                    verticalAlignment: Text.AlignVCenter
                    text: modelData

                    MouseArea {
                        anchors.fill : parent
                        enabled: index < menu.length - 1
                        hoverEnabled: cstyle.hoverEnabled
                        onEntered: {
                            control.color = cstyle.text.activeColor
                            control.font.underline = true
                            cursorShape = Qt.PointingHandCursor
                        }
                        onHoveredChanged: {
                            control.color = cstyle.blackColor
                            control.font.underline = false
                            cursorShape = Qt.ArrowCursor
                        }

                        onClicked:  {
                            menu.splice(index+1, menu.length - index - 1)
                            updated()
                        }
                    }
                }

                Text {
                    id: text
                    height: cstyle.height
                    width: cstyle.text.navigationHeight
                    text: index < menu.length - 1 ? ">" : ''
                    font.pixelSize: cstyle.fontSize
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
            }
        }
    }
}
