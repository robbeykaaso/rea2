import QtQuick 2.12
import QtQuick.Controls 2.5
import '../Style'

RadioButton {
    id: root
    property QtObject cstyle: DefaultStyle
    width: cstyle.width
    height: cstyle.height

    text: qsTr("hello")

    indicator: Rectangle {
        implicitWidth: cstyle.radio.outwidth(this)
        implicitHeight: cstyle.radio.outheight(this)

        anchors.verticalCenter: cstyle.verticalCenter(this)

        radius: cstyle.radio.radius(this)
        color: cstyle.whiteColor
        border.color: cstyle.radio.checkColor
        border.width: cstyle.radio.borderWidth(root.visualFocus)

        Rectangle {
            x: cstyle.radio.checkX(this)
            y: cstyle.radio.checkY(this)
            width: cstyle.radio.checkWidth(this)
            height: cstyle.radio.checkWidth(this)
            radius: cstyle.radio.radius(this)
            color: cstyle.radio.checkColor
            visible: root.checked
        }
    }
    contentItem: Text {
        text: parent.text
        font.pixelSize: cstyle.fontSize
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }
}
