import QtQuick 2.12
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import '../Style'

Row {
    property alias caption: caption
    property alias check: chk
    property double ratio: cstyle.ratio
    property QtObject cstyle: DefaultStyle
    width: cstyle.width
    height: cstyle.height

    spacing: cstyle.spacing(this)

    Text {
        id: caption
        text: "hello:"
        verticalAlignment: Text.AlignVCenter
        anchors.verticalCenter: cstyle.verticalCenter(this)
        font.pixelSize: cstyle.fontSize
        width: cstyle.text.width(this)
        height: cstyle.text.height(this)
    }

    Rectangle {
        width: cstyle.rect.width(this)
        height: cstyle.rect.height(this)
        color: cstyle.transparent
        CheckBox {
            id: chk
            width: cstyle.getWidth(this)
            height: cstyle.getHeight(this)
            style: cstyle.checkBox.style
        }
    }
}
