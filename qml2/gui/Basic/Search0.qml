import QtQuick 2.12
import '../Style'

FocusScope {
    id: wrapper
    property alias text: input.text
    property alias hint: hint.text
    property alias prefix: prefix.text
    property alias background: back
    property QtObject cstyle: DefaultStyle

    signal accepted

    Rectangle {
        id: back
        anchors.fill: parent
        border.color: cstyle.search.textColor
        color: cstyle.whiteColor
        radius: cstyle.search.radius

        MouseArea {
            anchors.fill: parent
            hoverEnabled: cstyle.hoverEnabled
            cursorShape: Qt.IBeamCursor
        }

        Text {
            id: hint
            anchors { fill: parent; leftMargin: cstyle.search.leftMargin }
            verticalAlignment: Text.AlignVCenter
            text: "Enter word"
            font.pixelSize: cstyle.search.fontSize
            color: cstyle.search.textColor
            opacity: cstyle.search.opacity(input.displayText.length)
        }

        Text {
            id: prefix
            anchors { left: cstyle.left(this); leftMargin: cstyle.search.leftMargin; verticalCenter: cstyle.verticalCenter(this) }
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: cstyle.search.fontSize
            color: cstyle.search.textColor
            opacity: !hint.opacity
        }

        TextInput {
            id: input
            focus: cstyle.focus
            anchors { left: cstyle.textField.left(prefix); right: cstyle.right(this); top: cstyle.top(this); bottom: cstyle.bottom(this) }
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: cstyle.search.fontSize
            color: cstyle.search.textColor
            selectByMouse: cstyle.selectByMouse
            onAccepted: {
                wrapper.accepted()
            }
        }

        Image {
            source: "../../resource/icon-search.png"
            anchors.right: cstyle.right(this)
            anchors.rightMargin: cstyle.search.rightMargin
            anchors.verticalCenter: cstyle.verticalCenter(this)
            MouseArea {
                anchors { fill: parent; margins: cstyle.search.margins }
                onClicked: {
                    wrapper.accepted()
                }
            }
        }
    }
}
