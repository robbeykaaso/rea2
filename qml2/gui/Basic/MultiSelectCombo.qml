import QtQuick 2.12
import QtQuick.Controls 2.12
import '../Style'

Row {
    property alias caption: caption
    property alias background: bak
    property alias combo: cmb
    property QtObject cstyle: DefaultStyle
    property double ratio: cstyle.ratio
    width: cstyle.width
    height: cstyle.height

    spacing: cstyle.spacing(this)
    Text {
        id: caption
        text: "hello:"
        verticalAlignment: Text.AlignVCenter
        anchors.verticalCenter: cstyle.verticalCenter(this)
        font.pixelSize: cstyle.fontSize
        width: cstyle.text.width(this)
        height: cstyle.text.height(this)
    }

    Rectangle {
        width: cstyle.rect.width(this)
        height: cstyle.rect.height(this)
        ComboBox {
            id: cmb
            width: cstyle.getWidth(this)
            height: cstyle.getHeight(this)
            anchors.verticalCenter: cstyle.verticalCenter(this)
            background: Rectangle {
                id: bak
                color: cstyle.whiteColor
            }
            model: ListModel {}
            delegate: Rectangle {
                width: cstyle.getWidth(this)
                height: cstyle.comboBox.multiHeight(cmb)
                Row {
                    spacing: cstyle.margins
                    anchors.fill: parent
                    anchors.margins: cstyle.margins
                    MultiSelectCheck {
                        id: chk
                        onChangeCheck: function() {
                            var sels = cmb.displayText.split("、")
                            var nm = name.toString()
                            var idx = sels.indexOf(nm)
                            if (checked){
                                if (idx < 0)
                                    sels.push(nm)
                            }else if (idx >= 0)
                                sels.splice(idx, 1)

                            var ret = ""
                            for (var i in sels)
                                if (sels[i]){
                                    if (ret !== "")
                                        ret += "、"
                                    ret += sels[i]
                                }
                            cmb.displayText = ret
                        }
                    }
                    Label {
                        text: name
                        width: cstyle.label.width(this, chk)
                        height: cstyle.getHeight(this)
                        elide: Text.ElideRight
                        verticalAlignment: Qt.AlignVCenter
                        horizontalAlignment: Qt.AlignHCenter
                    }
                }
            }
            Component.onCompleted: {
                displayText = ""
            }
        }
    }
}
