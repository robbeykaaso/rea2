import QtQuick 2.12
import QtQuick.Controls 2.5
import '../Style'

Column {
    id: column
    property QtObject cstyle: LogStyle
    property QtObject defaultstyle: DefaultStyle
    property alias titlebar: row
    property alias loglist: loglist
    property alias typelist: typelist
    property alias closepanel: closepanel
    property alias loglevel: loglevel

    property alias logmenu: logmenu
    property alias clear: clear
    height: cstyle.height
    width: cstyle.width
    Row {
        id: row
        width: defaultstyle.getWidth(this)
        height: cstyle.row.height(this)
        Row {
            width: cstyle.row.innerRow.width(this)
            height: defaultstyle.getHeight(this)
            spacing: cstyle.spacing

            Repeater {
                id: typelist
                model: ListModel {
                    ListElement {
                        cap: "system"
                        clr: "transparent"
                    }
                    ListElement {
                        cap: "train"
                        clr: "transparent"
                    }
                }

                delegate: Button {
                    width: cstyle.button.width(this)
                    height: defaultstyle.getHeight(this)
                    text: cap
                    background: Rectangle {
                        border.color:cstyle.borderColor
                        color: clr
                    }
                }
            }
        }
        Rectangle {
            height: defaultstyle.getHeight(this)
            width: cstyle.row.getWidth(this)
            color: defaultstyle.transparent
            border.color: cstyle.borderColor
            ComboBox {
                id: loglevel
                width: defaultstyle.getWidth(this)
                height: defaultstyle.getHeight(this)
                currentIndex: 0
                background: Rectangle {
                    color: defaultstyle.comboBox.backColor
                }
                contentItem: Text {
                    text: loglevel.displayText
                    color: defaultstyle.whiteColor
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
                model: [qsTr("Opt1"), qsTr("Opt2"), qsTr("Opt3")]
                delegate: ItemDelegate {
                    width: cstyle.getWidth(loglevel)
                    contentItem: Text {
                        text: modelData
                        color: defaultstyle.comboBox.backColor
                    }
                }
            }
        }
        Button {
            id: closepanel
            width: cstyle.row.getWidth(this)
            height: defaultstyle.getHeight(this)
            background: Rectangle {
                border.color: cstyle.borderColor
                color: defaultstyle.transparent
            }
            Text {
                text: "X"
                color: defaultstyle.whiteColor
                height: defaultstyle.getHeight(this)
                width: cstyle.row.getTextWidth(this)
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }

    ListView {
        width: defaultstyle.getWidth(this)
        height: cstyle.listview.height(this)
        x: cstyle.listview.getX(this)
        spacing: cstyle.listview.spacing
        topMargin: spacing
        clip: defaultstyle.clip
        model: ListModel {
            id: loglist
        }

        delegate: Text {
            text: msg
            color: defaultstyle.whiteColor
            font.pixelSize: cstyle.listview.text.fontSize
            wrapMode: Text.WordWrap
            width: defaultstyle.getWidth(this)
        }
        ScrollBar.vertical: ScrollBar {
        }
        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.RightButton
            Menu {
                id: logmenu
                MenuItem {
                    id: clear
                    text: qsTr("Clear")
                }
            }
        }
    }
}
