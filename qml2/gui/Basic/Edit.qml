import QtQuick 2.12
import QtQuick.Controls 2.5
import '../Style'

Row {
    property alias caption: caption
    property alias background: bak
    property alias input: val
    property double ratio: cstyle.ratio
    property QtObject cstyle: DefaultStyle
    width: cstyle.width
    height: cstyle.height

    spacing: cstyle.spacing(this)
    Text {
        id: caption
        text: "hello:"
        verticalAlignment: Text.AlignVCenter
        anchors.verticalCenter: cstyle.verticalCenter(this)
        font.pixelSize: cstyle.fontSize
        width: cstyle.text.width(this)
        height: cstyle.text.height(this)
    }

    Rectangle {
        id: bak
        color: cstyle.whiteColor
        width: cstyle.rect.width(this)
        height: cstyle.rect.height(this)
        anchors.verticalCenter: cstyle.verticalCenter(this)

        TextField {
            id: val
            width: cstyle.getWidth(this)
            height: cstyle.getHeight(this)
            color: cstyle.blackColor
            font.pixelSize: cstyle.fontSize
            anchors.leftMargin: cstyle.textField.leftMargin
            selectByMouse: cstyle.selectByMouse
            clip: cstyle.clip
            text: ""
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            background: Rectangle {
                width: parent.width
                color: cstyle.whiteColor
                border.color: parent.focus ? cstyle.textField.hoverBorderColor : cstyle.whiteColor
            }
        }
    }
}
