import QtQuick 2.0
import QtQuick.Controls 2.2
import '../Style'

Row {
    id: root

    property var modellist: []
    property string currentSelect: ''
    property string activeColor: 'Silver'

    property int currentIndex: -1
    property alias popupOpen: popup.visible
    property QtObject cstyle: DefaultStyle

    function getCurrentSelect() {
        return currentSelect
    }

    TextField {
        id: inputText
        text: currentSelect
        selectByMouse: cstyle.selectByMouse
        width: cstyle.textField.width(this)
        height: cstyle.getHeight(this)
        font.pixelSize: cstyle.fontSize
        background: Rectangle {
            color: cstyle.whiteColor
            border.color: parent.focus ? cstyle.textField.hoverBorderColor : cstyle.textField.borderColor
        }
        verticalAlignment: Text.AlignVCenter
        onTextChanged: {
            currentSelect = inputText.text
        }
    }

    Rectangle {
        height: cstyle.getHeight(this)
        width: cstyle.comboBox.image.getWidth(this, inputText)
        Image {
            id: openOrClose
            width: cstyle.comboBox.image.width
            height: root.height
            anchors.verticalCenter: cstyle.verticalCenter(this)
            anchors.horizontalCenter: parent.horizontalCenter
            transform: Rotation {
                origin.x: cstyle.comboBox.image.originX
                origin.y: cstyle.comboBox.image.originY(root)
                angle: popupOpen ? cstyle.comboBox.image.angle : cstyle.comboBox.image.defaultAngle
            }

            source: "../../resource/triangle-down.png"
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                popupOpen = !popupOpen
            }
        }
    }

    Popup {
        id: popup
        width: cstyle.comboBox.popup.width(root)
        y: cstyle.comboBox.popup.height(root)
        visible: cstyle.comboBox.popup.visible

        ListView {
            id: listview
            width: cstyle.getWidth(this)
            x: cstyle.comboBox.listView.x
            y: cstyle.comboBox.listView.y

            delegate: Item {
                id: item
                width: cstyle.comboBox.item.width(root)
                height: cstyle.comboBox.item.height(root)
                Rectangle {
                    width: cstyle.getWidth(this)
                    color: currentIndex === index ? activeColor : cstyle.whiteColor
                    height: cstyle.getHeight(this)
                    Text {
                        text: result
                        height: cstyle.getHeight(this)
                        verticalAlignment: Text.AlignVCenter
                        x: cstyle.comboBox.item.textX
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: cstyle.hoverEnabled
                        onEntered: currentIndex = index
                        onClicked: {
                            currentSelect = result
                            popupOpen = false
                        }
                    }
                }
            }
            model: ListModel {
            }
        }
    }
    onVisibleChanged: {
        if (!visible){
            popupOpen = false
        }
    }

    function updateGUI(){
        listview.model.clear()
        popup.height = modellist.length * root.height - 1
        listview.height = modellist.length * (root.height - 1)
        for (var i = 0; i < modellist.length; ++i) {
            listview.model.append({
                                      "result": modellist[i]
                                  })
            if (currentSelect === modellist[i]) {
                currentIndex = i
            }
        }
    }

    Component.onCompleted: {
        updateGUI()
    }
}
