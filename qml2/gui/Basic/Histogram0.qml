import QtQuick 2.12
import QtQuick.Controls 2.12
import QtCharts 2.3
import QtQuick.Window 2.12
import '../Style'

ChartView {
    property QtObject cstyle: DefaultStyle
    property QtObject thstyle: THistogramStyle

    width: cstyle.getWidth(this)
    height: cstyle.chartView.height(this)
    antialiasing:thstyle.antialiasing
    legend.alignment:Qt.AlignBottom
    legend.visible: thstyle.visible
    //titleColor: UIManager.fontColor
    //titleFont.pixelSize: UIManager.fontDespSize
    //titleFont.family: UIManager.fontFamily
    backgroundColor: cstyle.transparent
    ValueAxis{
        id: axisY
        min: thstyle.valueAxis1.min
        max: thstyle.valueAxis1.max
        //labelsColor: UIManager.fontColor
        labelsFont.pixelSize: thstyle.fontSize
        //labelsFont.family: UIManager.fontFamily
    }
    ValueAxis{
        id: axisX
        min: thstyle.valueAxis2.min
        max: thstyle.valueAxis2.max
        tickCount: thstyle.valueAxis2.tickCount
        //labelsColor: UIManager.fontColor
        labelsFont.pixelSize: thstyle.fontSize
        //labelsFont.family: UIManager.fontFamily
    }
    AreaSeries {
        axisX: axisX
        axisY: axisY
        color: thstyle.colorOne
        upperSeries: LineSeries{
            id: data
            /*XYPoint {x: 0; y: 5}
            XYPoint {x: 0.1; y: 5}
            XYPoint {x: 0.1; y: 8}
            XYPoint {x: 0.2; y: 8}
            XYPoint {x: 0.2; y: 2}
            XYPoint {x: 0.3; y: 2}*/
        }
        borderColor: cstyle.transparent
    }

    function drawHistomgram(aHistogram){
        data.clear()
        axisX.max = aHistogram.length
        var mx = 0
        for (var j in aHistogram){
            if (aHistogram[j] > mx)
                mx = aHistogram[j]
            var idx = parseInt(j)
            data.append(idx, 0)
            data.append(idx, aHistogram[j])
            data.append(idx + 1, aHistogram[j])
            data.append(idx + 1, 0)
        }
        axisY.max = mx + 1
    }
}
