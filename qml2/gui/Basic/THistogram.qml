import QtQuick 2.12
import QtCharts 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.4
import "../Basic"
import '../Style'

ChartView{
    property var histogram: []
    property bool oneThreshold: false
    property alias thresholdmax: maxthreshold
    property alias thresholdmin: minthreshold
    property QtObject cstyle: THistogramStyle
    property QtObject defaultstyle: DefaultStyle

    signal thresholdChanged(double aMaxThreshold, double aMinThreshold)

    title: "Score"
    antialiasing:cstyle.antialiasing
    legend.alignment:Qt.AlignBottom
    legend.visible: cstyle.visible
    //titleColor: UIManager.fontColor
    //titleFont.pixelSize: UIManager.fontDespSize
    //titleFont.family: UIManager.fontFamily
    backgroundColor: defaultstyle.transparent
    ValueAxis{
        id: axisY
        min: cstyle.valueAxis1.min
        max: cstyle.valueAxis1.max
        //labelsColor: UIManager.fontColor
        labelsFont.pixelSize: cstyle.fontSize
        //labelsFont.family: UIManager.fontFamily
    }
    ValueAxis{
        id: axisX
        min: cstyle.valueAxis3.min
        max: cstyle.valueAxis3.max
        tickCount: cstyle.valueAxis3.tickCount
        //labelsColor: UIManager.fontColor
        labelsFont.pixelSize: cstyle.fontSize
        //labelsFont.family: UIManager.fontFamily
    }
    AreaSeries {
        axisX: axisX
        axisY: axisY
        color: cstyle.colorOne
        upperSeries: LineSeries{
            id: goodarea
            XYPoint {x: cstyle.areaSeries1.x0; y: cstyle.areaSeries1.y0}
            XYPoint {x: cstyle.areaSeries1.x1; y: cstyle.areaSeries1.y1}
            XYPoint {x: cstyle.areaSeries1.x2; y: cstyle.areaSeries1.y2}
            XYPoint {x: cstyle.areaSeries1.x3; y: cstyle.areaSeries1.y3}
            XYPoint {x: cstyle.areaSeries1.x4; y: cstyle.areaSeries1.y4}
            XYPoint {x: cstyle.areaSeries1.x5; y: cstyle.areaSeries1.y5}
        }
        borderColor: defaultstyle.transparent
    }
    AreaSeries {
        axisX: axisX
        axisY: axisY
        color: cstyle.colorTwo
        upperSeries: LineSeries{
            id: interarea
            XYPoint {x: cstyle.areaSeries2.x0; y: cstyle.areaSeries2.y0}
            XYPoint {x: cstyle.areaSeries2.x1; y: cstyle.areaSeries2.y1}
        }
        borderColor: defaultstyle.transparent
    }
    AreaSeries {
        axisX: axisX
        axisY: axisY
        color: cstyle.colorThree
        upperSeries: LineSeries{
            id: badarea
            XYPoint {x: cstyle.areaSeries3.x0; y: cstyle.areaSeries3.y0}
            XYPoint {x: cstyle.areaSeries3.x1; y: cstyle.areaSeries3.y1}
            XYPoint {x: cstyle.areaSeries3.x2; y: cstyle.areaSeries3.y2}
            XYPoint {x: cstyle.areaSeries3.x3; y: cstyle.areaSeries3.y3}
            XYPoint {x: cstyle.areaSeries3.x4; y: cstyle.areaSeries3.y4}
            XYPoint {x: cstyle.areaSeries3.x5; y: cstyle.areaSeries3.y5}
        }
        borderColor: defaultstyle.transparent
    }
    AreaSeries{
        axisX: axisX
        axisY: axisY
        color: cstyle.colorFour
        opacity: cstyle.opacity
        upperSeries: LineSeries{
            id: mask
            XYPoint {x: cstyle.areaSeries4.x0; y: cstyle.valueAxis1.max}
            XYPoint {x: cstyle.areaSeries4.x1; y: cstyle.valueAxis1.max}
        }
        borderColor: defaultstyle.transparent
    }
    LineSeries{
        id: maxthreshold
        property bool focused: false
        property double x: 0.3
        axisX: axisX
        axisY: axisY
        color: cstyle.colorOne
        opacity: cstyle.opacity
        width: cstyle.width
        style: Qt.DotLine
        XYPoint {x: cstyle.lineSeries1.x; y: cstyle.lineSeries1.y}
        XYPoint {x: cstyle.lineSeries1.x; y: cstyle.valueAxis1.max}

        onHovered: {
            focused = !focused
            if (focused){
                msarea.cursorShape = Qt.PointingHandCursor
                msarea.selected = maxthreshold
                color = cstyle.lightGreen
            }else{
                msarea.cursorShape = Qt.ArrowCursor
                msarea.selected = undefined
                color = cstyle.colorOne
            }
        }

        function setX(aX){
            x = aX
            clear()
            append(x, 0)
            append(x, axisY.max)
            mask.clear()
            mask.append(x, axisY.max)
            mask.append(minthreshold.x, axisY.max)
        }

    }
    LineSeries{
        id: minthreshold
        property bool focused: false
        property double x: 0.5
        axisX: axisX
        axisY: axisY
        color: cstyle.colorThree
        opacity: cstyle.opacity
        width: cstyle.width
        style: Qt.DotLine
        XYPoint {x: cstyle.lineSeries2.x; y: cstyle.lineSeries2.y}
        XYPoint {x: cstyle.lineSeries2.x; y: cstyle.valueAxis1.max}

        onHovered: {
            focused = !focused
            if (focused){
                msarea.cursorShape = Qt.PointingHandCursor
                msarea.selected = minthreshold
                color = cstyle.pink
            }else{
                msarea.cursorShape = Qt.ArrowCursor
                msarea.selected = undefined
                color = cstyle.colorThree
            }
        }
        function setX(aX){
            x = aX
            clear()
            append(x, 0)
            append(x, axisY.max)
            mask.append(maxthreshold.x, axisY.max)
            mask.append(x, axisY.max)
        }
    }
    MouseArea{
        id: msarea
        property var selected
        anchors.fill: parent
        onPositionChanged: {
            //console.log(selected)
            //console.log(mouseX)
            var tmp = parent.mapToValue(Qt.point(mouseX, mouseY))
            if (selected === maxthreshold || selected === minthreshold)
                if (oneThreshold){
                    if (tmp.x >= 0 && tmp.x <= 1){
                        minthreshold.setX(tmp.x)
                        maxthreshold.setX(tmp.x)
                        drawHistoGram()
                        thresholdChanged(maxthreshold.x, minthreshold.x)
                    }
                }else if (selected === maxthreshold){
                    if (tmp.x >= 0 && tmp.x < minthreshold.x){
                        selected.setX(tmp.x)
                        drawHistoGram()
                        thresholdChanged(maxthreshold.x, minthreshold.x)
                    }
                }else if (selected === minthreshold){
                    if (tmp.x <= 1 && tmp.x > maxthreshold.x){
                        selected.setX(tmp.x)
                        drawHistoGram()
                        thresholdChanged(maxthreshold.x, minthreshold.x)
                    }
                }
        }
    }

    function clear(){
        goodarea.clear()
        interarea.clear()
        badarea.clear()
    }

    function drawHistoGram(aHistogram){
        if (aHistogram !== undefined)
            histogram = aHistogram
        if (!histogram || histogram.length == 0)
            return
        var interval = 1 / histogram.length
        /*var histogram = {}
        for (var i in scores){
            if (scores[i] === null)
                continue
            var k = Math.round(scores[i] / interval)
            if (k * interval >= 1)
                k--
            if (histogram[k] === undefined)
                histogram[k] = 0
            histogram[k]++;
        }*/

        clear()
        var mx = 0;
        for (var j in histogram){
            if (histogram[j] > mx)
                mx = histogram[j]
            var lft = parseInt(j) * interval
            var rt = (parseInt(j) + 1) * interval
            //console.log(rt)
            var area
            if (rt < maxthreshold.x){
                area = goodarea
            }else if (lft < minthreshold.x){
                area = interarea
            }else{
                area = badarea
            }
            area.append(lft, 0)
            area.append(lft, histogram[j])
            area.append(rt, histogram[j])
            area.append(rt, 0);
        }
        axisY.max = mx + 1
        minthreshold.setX(minthreshold.x)
        maxthreshold.setX(maxthreshold.x)
    }
    Component.onCompleted: {
        clear()
        if (oneThreshold)
            maxthreshold.setX(minthreshold.x)
    }
}
