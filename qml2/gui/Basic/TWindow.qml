import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Window 2.13
import QtQuick.Controls.Universal 2.3
import '../Basic'
import '../Style'

Window{
    id: root
    //flags: Qt.Dialog
    property string caption
    property alias content: mid.data
    property var titlebuttons
    property var footbuttons
    property bool titlevisible: true
    property string titlecolor: cstyle.titlecolor
    property string bottomcolor: bodycolor
    property string buttoncolor: cstyle.buttoncolor
    property string fontcolor: defaultstyle.blackColor
    property string bodycolor: cstyle.bodycolor
    property QtObject cstyle: WindowStyle
    property QtObject defaultstyle: DefaultStyle
    signal closed()
    flags: Qt.Window | Qt.CustomizeWindowHint //Qt.FramelessWindowHint
    modality: Qt.WindowModal
    width: cstyle.width
    height: cstyle.height
    color: bodycolor

    function show() {
        root.visible = true
        cont.visible = true
    }

    function close(c) {
        if (!c) {
            close(true)
            root.visible = false
        }
        cont.close()
    }

    Window{
        id: cont
        width: cstyle.contentWindow.width(root)
        x: cstyle.contentWindow.getX(root)
        height: cstyle.contentWindow.height(root)
        y: cstyle.contentWindow.getY(root)
        color: bodycolor
        visible: defaultstyle.visible
        flags: Qt.Window | Qt.FramelessWindowHint
        transientParent: root

        MouseArea{
            property int coor_x
            property int coor_y
            width: defaultstyle.getWidth(this)
            height: cstyle.titleHeight
            z: cstyle.mouseAreaZ
            onPressed: function(aInput){
                coor_x = aInput["x"]
                coor_y = aInput["y"]
            }
            onPositionChanged: function(aInput){
                root.x += aInput["x"] - coor_x
                root.y += aInput["y"] - coor_y
            }
        }

        Column{
            anchors.fill: parent
            Rectangle{
                width: defaultstyle.getWidth(this)
                height: cstyle.titleHeight
                color: bodycolor
                visible: !titlevisible
            }
            Rectangle{
                width: defaultstyle.getWidth(this)
                height: cstyle.titleHeight
                color: titlecolor
                visible: titlevisible
                Row{
                    anchors.fill: parent
                    Item{
                        x: cstyle.itemOne.getX(this)
                        y: cstyle.itemOne.getX(this)
                        height: cstyle.itemOne.height(this)
                        width: height
                        ShaderEffect{
                            id: logo
                            anchors.fill: parent
                            property variant source: Image{
                                anchors.fill: parent
                                source: "../../resource/logo.png"
                            }
                            property real amplitude: 0
                            property real frequency: 20
                            property real time: 0
                            NumberAnimation on time {
                                id: animation
                                running: false
                                loops: Animation.Infinite; from: cstyle.loopsFrom; to: cstyle.loopsTo; duration: cstyle.loopsDuration
                            }
                            /*fragmentShader: "
                                varying highp vec2 qt_TexCoord0;
                                uniform sampler2D source;
                                uniform lowp float qt_Opacity;
                                uniform lowp float redChannel;
                                void main(){
                                    gl_FragColor = texture2D(source, qt_TexCoord0) * vec4(redChannel, 1.0, 1.0, 1.0) * qt_Opacity;
                                }
                            "*/
                            fragmentShader: "
                                uniform sampler2D source;
                                varying highp vec2 qt_TexCoord0;
                                uniform lowp float qt_Opacity;
                                uniform highp float amplitude;
                                uniform highp float frequency;
                                uniform highp float time;
                                void main()
                                {
                                    highp vec2 p = sin(time + frequency * qt_TexCoord0);
                                    gl_FragColor = texture2D(source, qt_TexCoord0 + amplitude * vec2(p.y, -p.x)) * qt_Opacity;
                                }
                            "
                            MouseArea{
                                anchors.fill: parent
                                onClicked: {
                                    animation.running = !animation.running
                                    if (animation.running)
                                        logo.amplitude = 0.02
                                    else
                                        logo.amplitude = 0
                                }
                            }
                        }
                    }
                    Item{
                        id: titbar
                        height: defaultstyle.getHeight(this)
                        width: cstyle.itemTwo.width(this, btns, logo)
                        Text{
                            leftPadding: cstyle.itemTwo.leftPadding(this)
                            anchors.verticalCenter: defaultstyle.verticalCenter(this)
                            text: caption
                            font.pixelSize: defaultstyle.fontSize
                            color: fontcolor
                        }
                    }
                    Row{
                        width: childrenRect.width
                        height: defaultstyle.getHeight(this)

                        Repeater {
                            id: btns
                            model: ListModel {
                            }

                            delegate: Button {
                                width: defaultstyle.getHeight(this)
                                height: defaultstyle.getHeight(this)
                                contentItem: Text{
                                    text: cap
                                    color: parent.hovered ? buttoncolor : fontcolor
                                    font.pixelSize: defaultstyle.fontSize
                                    horizontalAlignment: Text.AlignHCenter
                                    verticalAlignment: Text.AlignVCenter
                                }
                                background: Rectangle{
                                    border.color: parent.hovered ? cstyle.buttoncolor : defaultstyle.transparent
                                    color: parent.hovered ? cstyle.itemTwo.buttonColor : defaultstyle.transparent
                                }
                            }
                        }

                        Component.onCompleted: {
                            if (titlebuttons){
                                for (var i in titlebuttons){
                                    btns.model.append({cap: titlebuttons[i]["cap"]})
                                    btns.itemAt(btns.count - 1).clicked.connect(titlebuttons[i]["func"])
                                }
                            }
                            btns.model.append({cap: "X"})
                            btns.itemAt(btns.count - 1).clicked.connect(function(){
                                closed()
                                close()
                            })
                        }
                    }
                }

            }
            Item{
                id: mid
                width: defaultstyle.getWidth(this)
                height: cstyle.item.height(this, footbuttons)
            }
            Rectangle {
                width: cstyle.contentWindow.width(root)
                height: footbuttons ? cstyle.bottomHeight : 0
                color: footbuttons ? bottomcolor : bodycolor
                Row{
                    anchors.right: defaultstyle.right(this)
                    anchors.rightMargin: cstyle.row.rowRightMargin(this)
                    width: childrenRect.width
                    height: footbuttons ? cstyle.row.rowHeight : cstyle.row.hiddenHeight
                    spacing: cstyle.spacing

                    Repeater{
                        id: btns2
                        model: ListModel{

                        }
                        delegate: Button{
                            height: cstyle.button.height(this)
                            width: cstyle.button.width(this)
                            contentItem: Text{
                                text: cap
                                color: parent.hovered ? buttoncolor : fontcolor
                                font.pixelSize: defaultstyle.fontSize
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                            }
                            anchors.verticalCenter: defaultstyle.verticalCenter(this)
                            background: Rectangle{
                                color: parent.hovered ? cstyle.button.activeBackColor : buttoncolor
                                border.color: parent.hovered ? buttoncolor : cstyle.button.activeBackColor
                            }
                        }
                    }

                    Component.onCompleted: {
                        if (footbuttons)
                            for (var i in footbuttons){
                                btns2.model.append({cap: footbuttons[i]["cap"]})
                                btns2.itemAt(btns2.count - 1).clicked.connect(footbuttons[i]["func"])
                            }
                    }
                }
            }
        }
    }

    onHeightChanged: {
        mid.height = height - Screen.desktopAvailableHeight * (0.045 + (footbuttons ? 0.03 : 0))
    }

    function setHeight(aHeight){
        height = aHeight
        mid.height = height - Screen.desktopAvailableHeight * (0.045 + (footbuttons ? 0.03 : 0))
    }
}
