import QtQuick 2.12
import QtQuick.Controls 2.5
import '../Style'

Row {
    property alias caption: caption
    property alias background: bak
    property alias combo: cmb
    property double ratio: cstyle.ratio
    property QtObject cstyle: DefaultStyle
    width: cstyle.width
    height: cstyle.height

    spacing: cstyle.spacing(this)
    Text {
        id: caption
        text: "hello:"
        verticalAlignment: Text.AlignVCenter
        anchors.verticalCenter: cstyle.verticalCenter(this)
        font.pixelSize: cstyle.fontSize
        width: cstyle.text.width(this)
        height: cstyle.text.height(this)
    }

    Rectangle {
        id: bak
        color: cstyle.whiteColor
        width: cstyle.rect.width(this)
        height: cstyle.rect.height(this)
        anchors.verticalCenter: cstyle.verticalCenter(this)
        EditCombo{
            id: cmb
            anchors.fill: parent
            height: parent.height
            activeColor: "lightskyblue"
        }
    }
}
