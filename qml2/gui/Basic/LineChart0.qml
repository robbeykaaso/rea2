import QtQuick 2.12
import QtCharts 2.3
import '../Style'

ChartView {
    property var content: [10, 20, 15, 30, 10, 60]
    property QtObject cstyle: DefaultStyle
    property QtObject thstyle: THistogramStyle

    antialiasing:thstyle.antialiasing
    legend.alignment:Qt.AlignBottom
    legend.visible: thstyle.visible
    //titleColor: UIManager.fontColor
    //titleFont.pixelSize: UIManager.fontDespSize
    //titleFont.family: UIManager.fontFamily
    backgroundColor: cstyle.transparent
    ValueAxis{
        id: valY
        //labelsColor: UIManager.fontColor
        labelsFont.pixelSize: thstyle.fontSize
        //labelsFont.family: UIManager.fontFamily
        min: thstyle.valueAxis1.min
    }
    ValueAxis{
        id: valX
        //labelsColor: UIManager.fontColor
        labelsFont.pixelSize: thstyle.fontSize
        //labelsFont.family: UIManager.fontFamily
        min: thstyle.valueAxis1.min
    }
    LineSeries {
        id: data
        axisX: valX
        axisY: valY
        XYPoint {x:thstyle.lineSeries.x0;y:thstyle.lineSeries.y0}
        XYPoint {x:thstyle.lineSeries.x1;y:thstyle.lineSeries.y1}
        XYPoint {x:thstyle.lineSeries.x2;y:thstyle.lineSeries.y2}
        XYPoint {x:thstyle.lineSeries.x3;y:thstyle.lineSeries.y3}
        XYPoint {x:thstyle.lineSeries.x4;y:thstyle.lineSeries.y4}
        XYPoint {x:thstyle.lineSeries.x5;y:thstyle.lineSeries.y5}
        XYPoint {x:thstyle.lineSeries.x6;y:thstyle.lineSeries.y6}
    }

    function updateGUI(){
        if (content === undefined)
            return;
        data.clear()
        var mx = 0;
        for (var i in content){
            if (content[i] > mx)
                mx = content[i]
            data.append(i, content[i]);
        }
        valX.max = content.length - 1
        valY.max = mx
    }

    function updateModel(aInput){
        content = aInput
        updateGUI()
    }

    Component.onCompleted: {
        updateGUI()
    }
}
