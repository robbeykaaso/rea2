import QtQuick 2.12
import QtQuick.Controls 1.2
import QtQuick.Controls 2.5
import QtQuick.Controls.Styles 1.2
import '../Style'

StatusBar {
    property alias statuslist: data
    property QtObject cstyle: DefaultStyle
    width: cstyle.statusbar.width
    height: cstyle.height
    style: cstyle.statusbar.style

    Row {
        anchors.fill: parent
        Repeater {
            model: ListModel {
                id: data
                ListElement {
                    cap: "status1xxxxxxxxxxxx"
                }
                ListElement {
                    cap: "status2"
                }
            }

            delegate: Row {
                height: cstyle.getHeight(this)

                Label {
                    text: cap
                    anchors.verticalCenter: cstyle.verticalCenter(this)
                }
                ToolSeparator {
                    //height: parent.height
                    anchors.verticalCenter: cstyle.verticalCenter(this)
                }
            }
        }
    }
}
