import QtQuick 2.12
import QtQuick.Controls 2.0
import '../Style'

Column {
    property QtObject cstyle: ListStyle
    property QtObject defaultCStyle: DefaultStyle
    property string selectSuffix: ""
    property var title: ["attr0", "attr1", "attr2"]
    property var selects: []
    property string selcolor: cstyle.selcolor
    property string dfltcolor: defaultCStyle.transparent
    property string titcolor: defaultCStyle.transparent
    property int titfontsize: cstyle.titfontsize
    property int itmfontsize: defaultCStyle.fontSize
    property var tr
    property alias entries: mdl
    signal selected

    property var tableLists: []

    onWidthChanged: updateChildrenWidth()

    function updateChildrenWidth() {
        for (var i = 0; i < rep.children.length; ++i) {
            rep.children[i].width = parent.width / title.length + (i == title.length - 1 ? 0 : 1)

            var lists = tableLists.filter(v => v.index === i)

            for (var j = 0; j < lists.length; ++j) {
                lists[j].component.width = rep.children[i].width
            }
        }
    }

    width: cstyle.width
    height: cstyle.height

    Row {
        width: defaultCStyle.getWidth(this)
        height: defaultCStyle.height
        spacing: cstyle.spacing
        id: rep

        Repeater {
            model: title.length
            delegate: Rectangle {
                id: titleRect
                border.color: defaultCStyle.blackColor
                width: cstyle.rect.width(this, title, index)
                height:defaultCStyle.getHeight(this)
                color: titcolor
                Text {
                    id: textV
                    text: tr(title[index])
                    height: defaultCStyle.getHeight(this)
                    width: cstyle.text.width(this)
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: titfontsize
                    elide: Text.ElideRight
                    clip: defaultCStyle.clip
                }

                Rectangle {
                    width: cstyle.rect.borderWidth
                    height: defaultCStyle.getHeight(this)
                    anchors.left: cstyle.right(textV)
                    visible: index !== title.length - 1

                    Rectangle {
                        width: cstyle.rect.borderWidth
                        height: defaultCStyle.getHeight(this)
                        color: defaultCStyle.blackColor
                    }

                    MouseArea {
                        hoverEnabled: defaultCStyle.hoverEnabled
                        width: cstyle.rect.borderWidth
                        height: defaultCStyle.getHeight(this)

                        property var origin_shape
                        property int coor_x
                        property int coor_y

                        onEntered: {
                            if (index !== title.length - 1) {
                                origin_shape = cursorShape
                                cursorShape = Qt.SplitHCursor
                            }
                        }
                        onExited: {
                            if (index !== title.length - 1) {
                                cursorShape = origin_shape
                            }
                        }
                        onPressed: function (aInput) {
                            coor_x = aInput["x"]
                            coor_y = aInput["y"]
                        }
                        onPositionChanged: function (aInput) {
                            if (aInput.buttons === Qt.LeftButton
                                    && index !== title.length - 1) {
                                var allWidth = rep.children[index].width
                                        + rep.children[index + 1].width

                                var x = aInput.x
                                if (x < 0 && Math.abs(
                                            x) < rep.children[index].width) {

                                    rep.children[index + 1].width
                                            = rep.children[index + 1].width + Math.abs(
                                                x)
                                    rep.children[index].width = allWidth
                                            - rep.children[index + 1].width
                                } else if (x > 0
                                           && x < rep.children[index + 1].width) {
                                    rep.children[index].width = rep.children[index].width + x
                                    rep.children[index + 1].width = allWidth
                                            - rep.children[index].width
                                }

                                for (var i = 0; i < rep.children.length; ++i) {
                                    var lists = tableLists.filter(v => v.index === i)

                                    for (var j = 0; j < lists.length; ++j) {
                                        lists[j].component.width = rep.children[i].width
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    ListView {
        id: view
        width: defaultCStyle.getWidth(this)
        height: cstyle.listview.height(this)
        clip: defaultCStyle.clip
        delegate: Rectangle {
            id: itm
            width: defaultCStyle.getWidth(this)
            height: defaultCStyle.height
            color: clr
            Row {
                anchors.fill: parent
                spacing: cstyle.spacing
                Repeater {
                    model: title.length
                    delegate: Text {
                        id: textR
                        width: cstyle.rect.width(this, title, index)
                        height: defaultCStyle.getHeight(this)
                        text: entry[index]
                        color: fontclr
                        font.pixelSize: itmfontsize
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                        clip: defaultCStyle.clip

                        Component.onCompleted: {
                            var item = {
                                "index": index,
                                "component": textR
                            }
                            tableLists.push(item)
                        }
                    }
                }
            }


            /*states: State {
                //name: "Current"
                when: itm.ListView.isCurrentItem
                PropertyChanges { target: itm; x: 0; scale: 1.1}
            }
            transitions: Transition {
                NumberAnimation { easing.type: Easing.Linear; properties: "scale"; duration: 200 }
            }*/
            MouseArea {
                anchors.fill: parent
                onClicked: function (aEvent) {
                    if (aEvent.modifiers === Qt.ShiftModifier) {
                        var st = Math.min(view.currentIndex,
                                          index), ed = Math.max(
                                                      view.currentIndex, index)
                        for (var j = selects.length - 1; j >= 0; j--)
                            if (selects[j] < st || selects[j] > ed) {
                                mdl.get(selects[j]).clr = dfltcolor
                                selects.splice(j, 1)
                            }
                        for (var k = st; k <= ed; ++k)
                            if (selects.indexOf(k) < 0) {
                                mdl.get(k).clr = selcolor
                                selects.push(k)
                            }
                    } else if (aEvent.modifiers === Qt.ControlModifier) {
                        if (index != view.currentIndex) {
                            var idx = selects.indexOf(index)
                            if (idx < 0) {
                                selects.push(index)
                                mdl.get(index).clr = selcolor
                            } else {
                                selects.splice(idx, 1)
                                mdl.get(index).clr = dfltcolor
                            }
                        }
                    } else {
                        for (var i in selects)
                            if (selects[i] !== view.currentIndex)
                                mdl.get(selects[i]).clr = dfltcolor
                        view.currentIndex = index
                        selects = [index]
                    }
                    selected()
                }
            }
        }
        highlight: Rectangle {
            width: parent ? defaultCStyle.getWidth(this) : cstyle.hightWidth
            height: defaultCStyle.height
            color: selcolor
            radius: cstyle.listview.radius
            border.width: cstyle.hightWidth
            //border.color: "#60f50a"
        }
        highlightFollowsCurrentItem: cstyle.highlightFollowsCurrentItem
        focus: defaultCStyle.focus
        model: ListModel {
            id: mdl
        }
        ScrollBar.vertical: ScrollBar {
        }
    }

    function updateList(aInput) {
        if (aInput["title"])
            title = aInput["title"]
        var entries = aInput["data"]
        if (aInput["selects"])
            selects = aInput["selects"]

        var cur = view.currentIndex
        var fontclr = aInput["fontclr"] || "black"
        if (aInput["index"]) {
            var idxes = aInput["index"]
            for (var j in entries) {
                var mdy = {
                    "entry": {

                    },
                    "clr": (selects.indexOf(
                                idxes[j]) > 0) ? selcolor : dfltcolor,
                    "fontclr": fontclr
                }
                var nw_ent = entries[j]["entry"]
                if (nw_ent) {
                    for (var l in nw_ent)
                        mdy["entry"][l] = nw_ent[l]
                } else
                    mdy["entry"] = mdl.get(idxes[j]).entry
                mdl.set(idxes[j], mdy)
            }
        } else {
            if (!aInput["append"])
                mdl.clear()
            for (var i in entries) {
                var ent = entries[i]["entry"]
                if (ent) {
                    var itm = {
                        "entry": {

                        },
                        "clr": (selects.indexOf(
                                    parseInt(i)) > 0) ? selcolor : dfltcolor,
                        "fontclr": fontclr
                    }
                    for (var k in ent)
                        itm["entry"][k] = ent[k]
                    mdl.append(itm)
                }
            }
        }
        if (aInput["selects"])
            view.currentIndex = selects.length > 0 ? selects[0] : -1
        else
            view.currentIndex = cur
    }
}
