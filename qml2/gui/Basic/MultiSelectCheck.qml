import QtQuick 2.12
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import '../Style'

CheckBox {
    id: chk
    height: cstyle.checkBox.height(this)
    width: cstyle.checkBox.width(this)
    anchors.verticalCenter: cstyle.verticalCenter(this)
    signal changeCheck()
    checked: seled || false
    property bool seled: false
    property QtObject cstyle: DefaultStyle
    style: cstyle.checkBox.style
    onCheckedChanged: changeCheck()
}
