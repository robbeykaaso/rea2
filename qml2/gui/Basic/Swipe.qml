import QtQuick 2.12
import QtQuick.Controls 2.5
import '../Style'

Item {
    property alias tabs: tabbar
    property alias pages: swipeView
    property QtObject cstyle: DefaultStyle
    width: cstyle.swipeview.swipeWidth
    height: cstyle.swipeview.swipeHeight
    TabBar {
        id: tabbar
        width: cstyle.getWidth(this)
        height: cstyle.height

        TabButton {
            text: qsTr("PageOne")
            font.pixelSize: cstyle.fontSize
        }
        TabButton {
            text: qsTr("PageTwo")
            font.pixelSize: cstyle.fontSize
        }
        TabButton {
            text: qsTr("PageTree")
            font.pixelSize: cstyle.fontSize
        }
        TabButton {
            text: qsTr("PageFour")
            font.pixelSize: cstyle.fontSize
        }
    }
    SwipeView {
        id: swipeView
        y: cstyle.swipeview.y
        width: cstyle.getWidth(this)
        height: cstyle.swipeview.height(this)

        currentIndex: tabbar.currentIndex

        Item {
            Label {
                text: qsTr("page one")
                anchors.centerIn: parent
                font.pixelSize: cstyle.swipeview.label.fontSize
                font.italic: cstyle.swipeview.label.fontItalic
                color: cstyle.swipeview.label.color
            }
        }
        Item {
            Label {
                text: qsTr("page two")
                anchors.centerIn: parent
                font.pixelSize: cstyle.swipeview.label.fontSize
                font.italic: cstyle.swipeview.label.fontItalic
                color: cstyle.swipeview.label.color
            }
        }
        Item {
            Label {
                text: qsTr("page three")
                anchors.centerIn: parent
                font.pixelSize: cstyle.swipeview.label.fontSize
                font.italic: cstyle.swipeview.label.fontItalic
                color: cstyle.swipeview.label.color
            }
        }
        Item {
            Label {
                text: qsTr("page four")
                anchors.centerIn: parent
                font.pixelSize: cstyle.swipeview.label.fontSize
                font.italic: cstyle.swipeview.label.fontItalic
                color: cstyle.swipeview.label.color
            }
        }
    }

    PageIndicator {
        id: indicator

        count: swipeView.count
        currentIndex: swipeView.currentIndex

        anchors.bottom: swipeView.bottom
        anchors.horizontalCenter: cstyle.horizontalCenter(this)
    }
}
