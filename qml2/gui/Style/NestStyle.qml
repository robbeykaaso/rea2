pragma Singleton
import QtQuick 2.12
import QtQuick.Controls 1.4

//Nesting QtObjects not work
//http://imaginativethinking.ca/make-qml-component-singleton/ qmldir
QtObject {
    property int rows: 5
    property int columns: 5
    property int rowSpacing: 0
    property int columnSpacing: 0
    property int fontSize: 16
    property QtObject result: QtObject {
        property int height: 60
        function width(aResult) {
            return aResult.parent.width * 0.9
        }
        function getHeight(aResult) {
            return aResult.parent.height - 30
        }
    }
}
