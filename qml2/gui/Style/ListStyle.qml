pragma Singleton
import QtQuick 2.12
import QtQuick.Controls 1.4

//Nesting QtObjects not work
//http://imaginativethinking.ca/make-qml-component-singleton/ qmldir
QtObject {
    property string selcolor: "lightskyblue"
    property int titfontsize: 14
    property int width: 100
    property int height: 200
    property int spacing: -1
    property int hightWidth: 0
    property bool highlightFollowsCurrentItem: true
    property var state: {
        "x": 0,
        "scale": 1.1
    }
    property var transition: {
        "properties": "scale",
        "duration": 200
    }
    function right(aRight) {
        return aRight.right
    }
    property QtObject rect: QtObject{
        property int borderWidth: 1
        function width(aRect, title, index){
            return aRect.parent.width / title.length + (index === title.length - 1 ? 0 : 1)
        }
    }
    property QtObject text: QtObject{
        function width(aText){
            return aText.parent.width - 1
        }
    }
    property QtObject listview: QtObject{
        property int radius: 5
        function height(aListview){
            return aListview.parent.height - 30
        }
        function getPageListHeight(aPagelist) {
            return aPagelist.parent.height - 60
        }
    }
}
