pragma Singleton
import QtQuick 2.12
import QtQuick.Controls 1.4

//Nesting QtObjects not work
//http://imaginativethinking.ca/make-qml-component-singleton/ qmldir
QtObject {
    property int fontSize: 10
    property string colorOne: '#1f9b98'
    property string colorTwo: 'yellow'
    property string colorThree: '#9e0a0a'
    property string colorFour: 'gray'
    property string lightGreen: "lightgreen"
    property string pink: "pink"
    property double opacity: 0.6
    property int width: 2
    property bool antialiasing: true
    property bool visible: false
    property var valueAxis1: {
        "min": 0,
        "max": 15
    }
    property var valueAxis2: {
        "min": 0,
        "max": 3,
        "tickCount": 6
    }
    property var valueAxis3: {
        "min": 0,
        "max": 1,
        "tickCount": 6
    }
    property var lineSeries: {
        "x0": 0,
        "y0": 2,
        "x1": 1,
        "y1": 2.3,
        "x2": 2,
        "y2": 1.5,
        "x3": 3,
        "y3": 1.7,
        "x4": 4,
        "y4": 1.3,
        "x5": 5,
        "y5": 1.1,
        "x6": 6,
        "y6": 0
    }
    property var lineSeries1: {
        "x": 0.3,
        "y": 0
    }
    property var lineSeries2: {
        "x": 0.5,
        "y": 0
    }
    property var areaSeries1: {
        "x0": 0,
        "y0": 5,
        "x1": 0.1,
        "y1": 5,
        "x2": 0.1,
        "y2": 8,
        "x3": 0.2,
        "y3": 8,
        "x4": 0.2,
        "y4": 2,
        "x5": 0.3,
        "y5": 2
    }
    property var areaSeries2: {
        "x0": 0.4,
        "y0": 8,
        "x1": 0.5,
        "y1": 8
    }
    property var areaSeries3: {
        "x0": 1.1,
        "y0": 6,
        "x1": 0.8,
        "y1": 6,
        "x2": 0.8,
        "y2": 10,
        "x3": 0.6,
        "y3": 10,
        "x4": 0.6,
        "y4": 3,
        "x5": 0.5,
        "y5": 3
    }
    property var areaSeries4: {
        "x0": 0.3,
        "x1": 0.5
    }
    property QtObject result: QtObject {
        function height(aResult) {
            return aResult.parent.height * 0.85
        }
    }
}
