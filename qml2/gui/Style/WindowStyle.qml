pragma Singleton
import QtQuick 2.12
import QtQuick.Controls 1.4
import QtQuick.Window 2.13

//Nesting QtObjects not work
//http://imaginativethinking.ca/make-qml-component-singleton/ qmldir
QtObject {
    property int width: 400
    property int height: 320
    property string titlecolor: "#ccc"//"steelblue"
    property string bottomcolor: "#ccc"
    property string bodycolor: "lightskyblue"
    property string buttoncolor: "gray"
    property int titleHeight: Screen.desktopAvailableHeight * 0.03
    property int spacing: 5
    property int mouseAreaZ: -1
    property int loopsFrom: 0
    property double loopsTo: Math.PI * 2
    property int loopsDuration: 600
    property int bottomHeight: 60
    property QtObject contentWindow: QtObject {
        function width(root) {
            return root.width
        }
        function height(root) {
            return root.height + 7
        }
        function getX(root) {
            return root.x
        }
        function getY(root) {
            return root.y - 7
        }
    }
    property QtObject item: QtObject {
        function height(aItem, footbuttons) {
            return aItem.parent.height - Screen.desktopAvailableHeight * (0.045 + (footbuttons ? 0.03 : 0))
        }
    }
    property QtObject itemOne: QtObject {
        function height(aItem) {
            return aItem.parent.height * 0.6
        }
        function getX(aItem) {
            return aItem.parent.height * 0.2
        }
    }
    property QtObject itemTwo: QtObject {
        property string buttonColor: '#ececec'
        function width(aItem, btns, logo) {
            return aItem.parent.width - btns.parent.width - logo.width
        }
        function leftPadding(aText) {
            return aText.parent.width * 0.05
        }
    }
    property QtObject row: QtObject {
        property int hiddenHeight: 0
        property int rowHeight: Screen.desktopAvailableHeight * 0.045
        function rowRightMargin(aRow) {
            return aRow.height * 0.4
        }
    }
    property QtObject button: QtObject {
        property string activeBackColor: "#d7d7d7"
        function height(aButton) {
            return aButton.parent.height * 0.5
        }
        function width(aButton) {
            return aButton.height * 2.5
        }
    }
}
