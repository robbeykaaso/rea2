pragma Singleton
import QtQuick 2.12
import QtQuick.Controls 1.4

//Nesting QtObjects not work
//http://imaginativethinking.ca/make-qml-component-singleton/ qmldir
QtObject {
    property int width: 130
    property QtObject button: QtObject{
        property int width: 20
        property int height: 20
        property int radius: 3
        property string backBorderColor: "gray"
    }
    property QtObject edit: QtObject {
        property int width: 60
        property double ratio: 0.5
    }
}
