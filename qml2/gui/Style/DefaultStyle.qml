pragma Singleton
import QtQuick 2.12
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

//Nesting QtObjects not work
//http://imaginativethinking.ca/make-qml-component-singleton/ qmldir
QtObject {
    property double ratio: 0.3
    property int width: 120
    property int height: 30
    property int margins: 5
    property int fontSize: 12
    property string transparent: 'transparent'
    property string blackColor: '#000'
    property string whiteColor: '#fff'
    property bool clip: true
    property bool selectByMouse: true
    property bool hoverEnabled: true
    property bool focus: true
    property bool visible: true
    property bool interactive: true
    property bool readOnly: true
    property bool active: true
    property bool fontBold: true
    property bool checked: true
    property bool reverse: false
    property int mouseAreaWidthHeight: 1
    property int opacity: 1
    function spacing(aSpin){
        return aSpin.width * 0.05
    }
    function getWidth(aWidth) {
        return aWidth.parent.width
    }
    function getHeight(aHeight) {
        return aHeight.parent.height
    }
    function left(aLeft) {
        return aLeft.parent.left
    }
    function right(aRight) {
        return aRight.parent.right
    }
    function top(aTop) {
        return aTop.parent.top
    }
    function bottom(aBottom) {
        return aBottom.parent.bottom
    }
    function verticalCenter(aVerticalCenter) {
        return aVerticalCenter.parent.verticalCenter
    }
    function horizontalCenter(aHorizontalCenter) {
        return aHorizontalCenter.parent.horizontalCenter
    }
    property QtObject text: QtObject{
        property string activeColor: 'blue'
        property int navigationHeight: 15
        function width(aText){
            return aText.parent.width * aText.parent.ratio
        }
        function height(aText){
            return aText.parent.height * 0.8
        }
    }
    property QtObject rect: QtObject{
        property int topPadding: 10
        property int pwindowWidth: 320
        property int pwindowHeight: 100
        function width(aRect){
            return aRect.parent.width * (1 - aRect.parent.ratio - 0.1)
        }
        function height(aRect){
            return aRect.parent.height * 0.8
        }
        function getPwindowWidth(aWindow) {
            return aWindow.parent.width - 20
        }
        property QtObject spinBox: QtObject{
            property int min: 0
            property int max: 200
            property Component style: SpinBoxStyle {
                background: Rectangle {
                    //radius: 6
                }
            }
        }
    }
    property QtObject gridRect: QtObject{
        property string activeColor: 'red'
        property int spacing: -1
        property int columns: 1
        property int rows: 1
        function width(aRect){
            return aRect.parent.width  / aRect.parent.columns
        }
        function height(aRect){
            return aRect.parent.height / aRect.parent.rows
        }
    }
    property QtObject chartView: QtObject{
        function height(aRect){
            return aRect.parent.height * 0.8
        }
    }
    property QtObject spinBox: QtObject {
        property int min: 0
        property int max: 200
        property string borderColor: 'gray'
        property string backColor: '#eee'
        property Component style: SpinBoxStyle {
            background: Rectangle {
                border.color: spinBox.borderColor
                border.width: 1
                radius: 2
                color: spinBox.backColor
            }
        }
        /*Rectangle {
            width: 100
            height: 30
            SpinBox {
                style: spinBox.style
                anchors.fill: parent
                font.pixelSize: spinBox.fontSize
                minimumValue: spinBox.min
                maximumValue: spinBox.max
            }
        }*/
    }
    property QtObject button: QtObject {
        property int width: 90
        property int fontSize: 14
        property double opacity: 0.5
        property string color: '#F2F2F2'
        property string activeColor: 'steelblue'
        property string borderColor: 'gray'
        property string backColor: 'gainsboro'
        property int margin: 5

        /*Button {
            width: cstyle.button.width
            height: cstyle.height
            contentItem: Text{
                text: 'cap'
                color: parent.hovered ? cstyle.button.color : cstyle.button.activeColor
                font.pixelSize: cstyle.button.fontSize
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            background: Rectangle{
                border.color: parent.hovered ? cstyle.transparent : cstyle.button.activeColor
                color: parent.hovered ? cstyle.button.activeColor : cstyle.transparent
            }
        }*/
    }
    property QtObject comboBox: QtObject {
        property string backColor: 'steelblue'
        function multiHeight(aCombobox) {
            return aCombobox.height + margins
        }
        property QtObject image: QtObject {
            property int width: 10
            property int originX: 5
            property int angle: 180
            property int defaultAngle: 0
            function originY(aImage) {
                return aImage.height / 2
            }
            function getWidth(aImage, text) {
                return aImage.parent.width - text.width
            }
        }
        property QtObject listView: QtObject {
            property int x: -11
            property int y: -11
        }
        property QtObject popup: QtObject {
            property bool visible: false
            function width(aRoot) {
                return aRoot.width
            }
            function height(aRoot) {
                return aRoot.height
            }
        }

        property QtObject item: QtObject {
            property int textX: 10
            function width(aRoot) {
                return aRoot.width - 2
            }
            function height(aRoot) {
                return aRoot.height - 1
            }
        }
        /*Rectangle {
          width: 200
          height: 30
                ComboBox {
                            id: combobox
                            width: cstyle.comboBox.width(this)
                            height: cstyle.comboBox.height(this)
                            background: Rectangle {
                                color: cstyle.comboBox.backColor
                            }
                            contentItem: Text {
                                text: combobox.displayText
                                color: cstyle.comboBox.textColor
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                            }
                            model: ['banana', 'orange']
                            delegate: ItemDelegate {
                                width: combobox.width
                                contentItem: Text {
                                    text: modelData
                                    color: cstyle.comboBox.backColor
                                }
                            }
                        }
}*/
    }
    property QtObject textField: QtObject {
        property string borderColor: 'gray'
        property string hoverBorderColor: 'steelblue'
        property int leftMargin: 5
        function width(aTextField){
            return aTextField.parent.width * 0.8
        }
        function left(aTextField) {
            return aTextField.right
        }

        /*Rectangle {
        width: 100
        height: 30
        TextField {
            width: style.textField.width(this)
            height: style.textField.height(this)
            color: style.blackColor
            background: Rectangle {
                width: parent.width
                color: style.whiteColor
                border.color: parent.hovered ? style.textField.hoverBorderColor : style.textField.borderColor
            }
        }
    }*/
    }
    property QtObject checkBox: QtObject {
        property int langrageSize: 20
        property string borderColor: 'gray'
        property string activeColor: 'steelblue'
        function width(aCheckbox) {
            return aCheckbox.parent.width * 0.2
        }
        function height(aCheckbox) {
            return aCheckbox.parent.height * 0.8
        }
        property Component style: CheckBoxStyle {
            indicator: Rectangle {
                id: functChose
                implicitWidth: checkBox.langrageSize
                implicitHeight: checkBox.langrageSize
                border.color: checkBox.borderColor
                border.width: 2
                radius: width / 4
                Rectangle {
                    visible: control.checked
                    color: checkBox.activeColor
                    border.color: checkBox.activeColor
                    anchors.margins: 5
                    anchors.fill: parent
                    radius: width / 4
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: control.checked = !control.checked
                }
            }
            label: Label {
                id: string
                text: control.text
                color: checkBox.borderColor
                font.pixelSize: fontSize
            }
        }

        /*CheckBox {
            id: test
            style: style.checkBox.style
            checked: style.checkBox.check(this)
            onCheckedChanged: {
                style.checkBox.check(this)
            }
        }*/
    }
    property QtObject tabbar: QtObject {
        property string backColor: 'lightskyblue'
        property string activeBackColor: 'steelblue'

        /*TabBar {
            id: bar
            background: Rectangle {
                color: cstyle.tabbar.backColor
            }
            Repeater {
                model: ["banana", "apple"]
                TabButton {
                    background: Rectangle {
                        color: bar.currentIndex === index ? cstyle.whiteColor : cstyle.tabbar.backColor
                    }
                    contentItem: Text {
                        text: modelData
                        color: bar.currentIndex === index ? cstyle.tabbar.activeFontColor : cstyle.blackColor
                    }
                }
            }
        }*/
    }
    property QtObject tabview: QtObject {
        property string backColor: 'lightskyblue'
        property string activeBackColor: 'steelblue'
        property Component style: TabViewStyle {
            frameOverlap: 1
            tab: Rectangle {
                color: styleData.selected ? tabview.activeBackColor : tabview.backColor
                border.color: tabview.activeBackColor
                implicitWidth: Math.max(text.width + 4, 80)
                implicitHeight: 30
                radius: 2
                Text {
                    id: text
                    anchors.centerIn: parent
                    text: styleData.title
                    color: styleData.selected ? whiteColor : blackColor
                    font.pixelSize: fontSize
                }
            }
            frame: Rectangle {
                color: whiteColor
            }
        }
    }
    property QtObject toolbar: QtObject {
        property int imHeight: 100
        property int imWidth: 100
        property string borderColor: 'gray'
        property string darkColor: 'steelblue'
        property Component style: ToolBarStyle {
            padding {
                left: 8
                right: 10
                top: 8
                bottom: 10
            }
            background: Rectangle {
                //                border.color: borderColor
                implicitHeight: toolbar.imHeight
                implicitWidth: toolbar.imWidth
                gradient: Gradient {
                    GradientStop {
                        position: 0
                        color: whiteColor
                    }
                    GradientStop {
                        position: 1
                        color: toolbar.darkColor
                    }
                }
            }
        }
    }
    property QtObject slider: QtObject {
        property int interval: 4
        property int minimumValue: 0
        property int maximumValue: 100
        property Component style: SliderStyle {
            handle: Rectangle {
                implicitWidth: 8
                implicitHeight: 12
                color: control.pressed ? "#1f9b98" : 'gray'
                radius: 8
            }
        }
    }
    property QtObject radio: QtObject {
        property string borderColor: '#eee'
        property string checkColor: 'gray'
        function outwidth(aRadio) {
            return aRadio.parent.height * 0.6
        }
        function outheight(aRadio) {
            return aRadio.parent.height * 0.6
        }
        function radius(aRadio) {
            return aRadio.width / 2
        }
        function checkX(aRadio) {
            return (aRadio.parent.width - aRadio.width) / 2
        }
        function checkY(aRadio) {
            return (aRadio.parent.height - aRadio.height) / 2
        }
        function checkWidth(aRadio) {
            return aRadio.parent.width * 0.7
        }
        function checkHeight(aRadio) {
            return aRadio.parent.width * 0.7
        }
        function borderWidth(judge) {
            return judge ? 2 : 1
        }
        /*RadioButton {
        id: test
        width: style.radio.width
        height: style.radio.height
        text: qsTr("hello")

        indicator: Rectangle {
            implicitWidth: style.radio.outwidth(this)
            implicitHeight: style.radio.outheight(this)
            anchors.verticalCenter: parent.verticalCenter

            radius: style.radio.radius(this)
            color: style.whiteColor
            border.color: style.radio.checkColor

            Rectangle {
                x: style.radio.checkX(this)
                y: style.radio.checkY(this)
                width: style.radio.checkWidth(this)
                height: style.radio.checkWidth(this)
                radius: style.radio.radius(this)
                color: style.radio.checkColor
                visible: test.checked
            }
        }
        contentItem: Text {
            text: parent.text
            font.pixelSize: style.radio.fontSize
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }*/
    }
    property QtObject progressbar: QtObject {
        property int width: 100
        property int height: 10
        property string backColor: 'lightgray'
        property string backBorderColor: 'gray'
        property string progressBorderColor: "steelblue"
        property string progressColor: "lightsteelblue"
        property Component style: ProgressBarStyle {
            background: Rectangle {
                radius: 2
                color: progressbar.backColor
                border.color: progressbar.backBorderColor
                border.width: 1
                implicitWidth: 200
                implicitHeight: 24
            }
            progress: Rectangle {
                border.color: progressbar.progressBorderColor
                color: progressbar.progressColor

                // Indeterminate animation by animating alternating stripes:
                Item {
                    anchors.fill: parent
                    anchors.margins: 1
                    visible: control.indeterminate
                    clip: clip
                    Row {
                        Repeater {
                            Rectangle {
                                color: index % 2 ? progressbar.progressBorderColor : progressbar.progressColor
                                width: 20
                                height: control.height
                            }
                            model: control.width / 20 + 2
                        }
                        XAnimator on x {
                            from: 0
                            to: -40
                            loops: Animation.Infinite
                            running: control.indeterminate
                        }
                    }
                }
            }
        }
        /*ProgressBar {
            value: 0.1
            width: progressbar.width
            height: progressbar.height
            style: progressbar.style
        }*/
    }
    property QtObject statusbar: QtObject {
        property string darkColor: 'lightsteelblue'
        property int width: 300
        property Component style: StatusBarStyle {
            padding {
                left: 8
                right: 8
                top: 3
                bottom: 3
            }
            background: Rectangle {
                implicitHeight: 16
                implicitWidth: 200
                gradient: Gradient {
                    GradientStop {
                        color: whiteColor
                        position: 0
                    }
                    GradientStop {
                        color: statusbar.darkColor
                        position: 1
                    }
                }
                Rectangle {
                    anchors.top: parent.top
                    width: parent.width
                    height: 1
                    color: whiteColor
                }
            }
        }
    }
    property QtObject label: QtObject {
        function width(aLabel, chk) {
            return aLabel.parent.width - chk.width
        }
    }
    property QtObject item: QtObject {
        property double ratio: 0.4
        property int width: 180
        property int leftPadding: 10
    }
    property QtObject search: QtObject {
        property int fontSize: 18
        property string textColor: '#707070'
        property int rightMargin: 12
        property int margins: -10
        property int radius: 4
        property int leftMargin: 14
        function opacity(judge) {
            return judge ? 0 : 1
        }
    }
    property QtObject swipeview: QtObject {
        property int y: 30
        property int swipeWidth: 300
        property int swipeHeight: 500
        function height(aRow) {
            return aRow.parent.height - swipeview.y
        }

        property QtObject label: QtObject {
            property int fontSize: 30
            property bool fontItalic: true
            property string color: 'yellow'
        }
    }
    property QtObject progress: QtObject {
        property int width: 300
        property int height: 100
        property int from: 0
        property int to: 1
        function spacing(aProgress) {
            return aProgress.height * 0.1
        }
        function leftPadding(aProgress) {
            return aProgress.parent.width * 0.1
        }
        function coWidth(aColumn) {
            return aColumn.parent.width * 0.8
        }
        function textHeight(aText) {
            return aText.parent.height * 0.3
        }
        function getHeight(aProgress) {
            return aProgress.parent.height * 0.5
        }
    }
}
