pragma Singleton
import QtQuick 2.12
import QtQuick.Controls 1.4

//Nesting QtObjects not work
//http://imaginativethinking.ca/make-qml-component-singleton/ qmldir
QtObject {
    property int spacing: -1
    property string borderColor: 'gray'
    property int width: 540
    property int height: 180
    function getWidth(aWidth) {
        return aWidth.width
    }
    property QtObject row: QtObject{
        function height(aRow){
            return aRow.parent.height * 0.3
        }
        function getWidth(aRow) {
            return aRow.parent.width * 0.2
        }
        function getTextWidth(aText) {
            return aText.parent.width * 0.5
        }

        property QtObject innerRow: QtObject{
            function width(aInnerRow){
                return aInnerRow.parent.width * 0.8 - aInnerRow.parent.height
            }
        }
    }
    property QtObject listview: QtObject {
        property int spacing: 2
        function width(aListview){
            return aListview.parent.width * 0.98
        }
        function height(aListview){
            return aListview.parent.height * 0.7
        }
        function getX(aListview) {
            return aListview.parent.width * 0.02
        }
        property QtObject text: QtObject {
            property int fontSize: 16
        }
    }
    property QtObject button: QtObject {
        function width(aButton) {
            return aButton.parent.width * 0.2
        }
    }
}
