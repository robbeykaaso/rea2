pragma Singleton
import QtQuick 2.12
import QtQuick.Controls 1.4
import QtQuick.Window 2.12

//Nesting QtObjects not work
//http://imaginativethinking.ca/make-qml-component-singleton/ qmldir
QtObject {
    property string selectColor: 'red'
    property int rootWidth: 600
    property int rootHeight: 600
    property int spacing: 5
    property int leftPadding: 10
    property QtObject rect: QtObject {
        property int height: 20
        property int itemHeight: 1
        property string color: 'gray'
        property QtObject image: QtObject {
            property int width: 10
            property int height: 10
            function left(aLeft) {
                return aLeft.right
            }
            function leftMargin(judge) {
                return judge ? 5 : 0
            }
            function rightMargin(judge) {
                return judge ? 20 : 0
            }
        }
        property QtObject item: QtObject {
            property int x: 0
            property int y: 0
            function width(defaultHeight, judge) {
                return defaultHeight + (judge ? 0 : 20)
            }
        }
    }
    property QtObject row: QtObject {
        property int height: 25
    }
    property QtObject button: QtObject {
        property int fontSize: 14
        property int width: 12
    }
    property QtObject window: QtObject {
        property int width: Screen.desktopAvailableWidth * 0.22
        property int height: Screen.desktopAvailableHeight * 0.22
        property int scrollHeight: 200
        property int columnHeight: 150
        function spacing(aColumn) {
            return aColumn.height * 0.05
        }
    }
    property QtObject scrollview: QtObject {
        property int contentHeight: 0
        property int treeNodeHeight: 500
        function height(aScrollview) {
            return aScrollview.parent.height - 35
        }
    }
}
